var _ = require('lodash');
var Converter = require("csvtojson").Converter;
var moment = require('moment')

var results = [];

// extract the data required of the raw record data
var converter = new Converter({});
converter.fromFile("./logfile.csv", function(err,result){
  result.forEach(function(entry) {

  var event = {};
  event.timestamp = entry["(PDH-CSV 4"]["0) (W"][" Europe Daylight Time)(-120)"];
  event.processorTimePercent = entry["\\\\WIN-8P359K38PHQ\\Processor(_Total)\\% Processor Time"];
  event.bytesSentPerSec = entry["\\\\WIN-8P359K38PHQ\\Network Adapter(Microsoft Hyper-V Network Adapter)\\Bytes Sent/sec"];
  event.bytesReceivedPerSec = entry["\\\\WIN-8P359K38PHQ\\Network Adapter(Microsoft Hyper-V Network Adapter)\\Bytes Received/sec"];
  event.logicalDisk = {};
  event.logicalDisk.diskWriteTime = entry["\\\\WIN-8P359K38PHQ\\LogicalDisk(_Total)\\% Disk Write Time"];
  event.logicalDisk.diskReadTime = entry["\\\\WIN-8P359K38PHQ\\LogicalDisk(_Total)\\% Disk Read Time"];
  event.logicalDisk.diskTime = entry["\\\\WIN-8P359K38PHQ\\LogicalDisk(_Total)\\% Disk Time"];
  event.logicalDisk.idleTime = entry["\\\\WIN-8P359K38PHQ\\LogicalDisk(_Total)\\% Idle Time"];
  event.availableRamMbytes = entry["\\\\WIN-8P359K38PHQ\\Memory\\Available MBytes"];

  results.push(event);
  });

// record duration
  var duration = getDiffTime(results[0].timestamp, results[110].timestamp);
  var minutes = Math.round(duration / 1000 / 60);
  var seconds = Math.round((duration / 1000) % 60);
  console.log(minutes + " minutes and " + seconds + " seconds were recorded")
  console.log("---------------------------------------------------------------")


  function printPlotData() {
    var data = _.drop(results, 1);
    console.log("time      gb      processor      networkIn      networkOut      diskTime      diskTimeThreshold")
    data.forEach(function(res) {
      console.log(
        ((getDate(res.timestamp).minutes() % 29) + getDate(res.timestamp).seconds() / 60) +
        "      " +
        (20 - (res.availableRamMbytes/1000)) +
        "      " +
        res.processorTimePercent +
        "      " +
        (res.bytesReceivedPerSec / 125000) + // 1 MByte === 125000 Bytes
        "      " +
        (res.bytesSentPerSec / 125000) +
        "      " +
        (res.logicalDisk.diskTime / 100) +
        "      " +
        2
      );
    });
  }
  printPlotData();

  //console.log(result[1])

});

function getDate(dateString) {
  return moment(new Date(dateString));
}

function getDiffTime(proTime, postTime) {
  return getDate(postTime) - getDate(proTime);
}
